if (!process.env.PORT)
  process.env.PORT = 8080;


// Priprava strežnika
var express = require('express');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));


// Lestvica najboljših igralcev
var lestvica = [
  { cas: "2019-06-19T10:43:07.513Z", vzdevek: "Trump the greatest", stVprasanj: 3, rezultat: 12 },
  { cas: "2019-06-19T09:00:06.856Z", vzdevek: "Mighty Persia", stVprasanj: 5, rezultat: 75 },
  { cas: "2019-06-19T07:00:00.000Z", vzdevek: "Fat boy Kim", stVprasanj: 3, rezultat: 12 },
  { cas: "2019-06-19T12:00:00.000Z", vzdevek: "Olaf", stVprasanj: 7, rezultat: 12 }
];


// Vprašanja z odgovori
var vprasanja = [
	{ "vprašanje": "Gospodarsko, kulturno in industrijsko središče Vipavske doline.",
		"odgovor": "Ajdovščina" },
	{ "vprašanje": "Jezero z otokom.",
		"odgovor": "Bled" },
	{ "vprašanje": "Mesto ob Savinji.",
		"odgovor": "Celje" },
	{ "vprašanje": "Obalno mesto ob Simonovem zalivu.",
		"odgovor": "Izola" },
	{ "vprašanje": "Iz tega mesta je prihajala miss Jugoslavije leta 1965, Nataša Košir.",
		"odgovor": "Kočevje" },
	{ "vprašanje": "Pivo in cvetje.",
		"odgovor": "Laško" },
	{ "vprašanje": "Slovenska prestolnica.",
		"odgovor": "Ljubljana" },
	{ "vprašanje": "Prestolnica Dolenjske.",
		"odgovor": "Novo mesto" },
	{ "vprašanje": "Kraj s koncertno dvorano v velikosti 3.000 m2 v kraški jami.",
		"odgovor": "Postojna" },
	{ "vprašanje": "Kraj, kjer vsako leto poteka kurentovanje.",
		"odgovor": "Ptuj" }
];


// Kraji s pripadajočimi GPS koordinatami
var kraji = {
	"Ajdovščina": { "lat": 45.887389, "lng": 13.901444 },
	"Bled": { "lat": 46.368803, "lng": 14.113975 },
	"Celje": { "lat": 46.24, "lng": 15.27 },
	"Izola": { "lat": 45.537864, "lng": 13.661769 },
	"Ljubljana": { "lat": 46.05, "lng": 14.5 },
	"Novo mesto": { "lat": 45.798578, "lng": 15.173911 },
	"Postojna": { "lat": 45.775864, "lng": 14.213661 },
	"Ptuj": { "lat": 46.416667, "lng": 15.866667 },
	"Kočevje": { "lat": 45.642961, "lng": 14.859383 },
	"Laško": { "lat": 46.156303, "lng": 15.238617 }
};


/**
 * Generiraj naključno število med vključno min in vključno max
 *
 * @param min najmanjša vrednost
 * @param max največja vrednost
 */
function nakljucnoStevilo(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}


/**
 * Generiraj n enoličnih naključnih števil med vključno min in vključno max
 *
 * @param n število enoličnih naključnih števil
 * @param min najmanjša vrednost
 * @param max največja vrednost
 */
function nakljucnaStevila(n, min, max) {
  if (n > max - min + 1) {
    return Error("Med " + min + " in " + max + " je mogoče generirati zgolj " +
      (max - min + 1) + " enoličnih naključnih števil!");
  }
  var rezultat = [];
  while (rezultat.length < n) {
    var stevilo = nakljucnoStevilo(min, max);
    if (rezultat.indexOf(stevilo) === - 1) rezultat.push(stevilo);
  }
  return rezultat;
}


// Generiranje igre s poljubnim številom vprašanj,
// ki ne more biti večje od vseh vprašanj v sistemu.
streznik.get("/igra/:steviloVprasanj", function(zahteva, odgovor) {
  var steviloVprasanj = parseInt(zahteva.params.steviloVprasanj, 10);
  if (steviloVprasanj == undefined || isNaN(steviloVprasanj)) {
    odgovor.send(404, "Manjka ustrezno število vprašanj!");
  } else if (steviloVprasanj > vprasanja.length) {
    odgovor.send(404, "Vnesite manjše število vprašanj, " +
      "saj jih je na voljo največ " + vprasanja.length + "!");
  } else {
    var rezultat=[steviloVprasanj];
    // generiramo nakljucna stevila 
    var stevila=[]; 
    stevila = nakljucnaStevila(steviloVprasanj, 1, vprasanja.length);
    
    for(var i=0; i<steviloVprasanj; i++){
      //kreiramo posamezno vprašanje 
     // rezultat[i].Vprašanje=vprasanja[stevila[i]].vprašanje;
    //  rezultat[i].Pravilni_odgovor=vprasanja[stevila[i]].odgovor;
      var ta_kraj = vprasanja[stevila[i]].odgovor;
      var a;
      var b; 
      for(var j=0; j<kraji.length; j++){
        if(kraji[j]==ta_kraj){
          a = kraji[j].lat;
          b = kraji[j].lng;
        }
        
      }
      let zacasno = {
         "Vprašanje": vprasanja[stevila[i]].vprašanje,
         "Pravilni odgovor": vprasanja[stevila[i]].odgovor,
         "Pravilne koordinate": {
    		  "lat": a,
    		  "lng": b,
          	}
        };
      rezultat[i].push(zacasno);
      
      
    }
   /* var rezultat = [{
      "Vprašanje": "Mesto ob Savinji.",
      "Pravilni odgovor": "Celje",
      "Pravilne koordinate": {
    		"lat": 46.24,
    		"lng": 15.27
    	}
    }];*/
    odgovor.send(rezultat);
  }
});


// Prikaz zemljevida s vprašanji
streznik.get("/", function (zahteva, odgovor) {
  odgovor.redirect('/zemljevid-vprasanja');
});


// Shranjevanje rezultatov igranja
streznik.get("/lestvica/:json", function(zahteva, odgovor) {
  var rezultat = zahteva.params.json;
  lestvica.push(JSON.parse(rezultat));
  odgovor.send("OK");
});


// Prikaz lestvice najboljših
streznik.get("/lestvica", function (zahteva, odgovor) {
//console.log(lestvica);

  odgovor.render("lestvica",{lestvica: lestvica});

  

});


// Prikaz strani z zemljevidom in vprašanji
streznik.get("/zemljevid-vprasanja", function (zahteva, odgovor) {
  odgovor.render('zemljevid-vprasanja');
});


streznik.listen(process.env.PORT, function () {
  console.log("Strežnik je pognan!");
});
